import requests
import torch
import torchvision.transforms as transforms
import uuid
import os
from datetime import datetime

from models.bart_txt2img import BartForConditionalGenerationTxt2Img

from flask import Flask, request, jsonify, abort, json

app = Flask(__name__)


@app.route('/api/generate', methods=['POST'])
def generate():

    data = request.json

    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

    text_prompt = (data['text_prompt'] if 'text_prompt' in data else '').strip()
    uri = (data['uri'] if 'uri' in data else '').strip()

    if len(uri) == 0 or len(text_prompt) == 0:
        abort(400)

    model = BartForConditionalGenerationTxt2Img.from_pretrained(f'/app/models/{uri}/pretrained/bart_txt2img').to(device)
    model.eval()

    img = model.inference(text_prompt)

    data['generated_at'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    img_path = f'/tmp/{uuid.uuid4()}.jpg'
    transforms.ToPILImage()(img).convert('RGB').save(img_path, 'JPEG')

    files = {
        'json': ('json_data', json.dumps(data), 'application/json'),
        'image': ('image.jpg', open(img_path, 'rb'), 'image/jpeg')
    }

    response = requests.post('http://webservice_tg_library:5002/api/items/upload', files=files)

    os.remove(img_path)

    if response.status_code != 200:
        abort(response.status_code)

    text_json = json.loads(response.text)

    return jsonify({ 'uri': text_json['uri'] })
