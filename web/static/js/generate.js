document.getElementById('generate-form').addEventListener('submit', function(event) {
    event.preventDefault();

    const formData = new FormData(this);

    const csrfMetaTag = document.querySelector('meta[name="csrf-token"]');

    const headers = {}

    if (csrfMetaTag) {
        const csrfToken = csrfMetaTag.getAttribute('content');
        headers['X-CSRF-Token'] = csrfToken;
    }

    fetch('/generate', {
        method: 'POST',
        headers: headers,
        body: formData,
    })
    .then(response => response.text())
    .then(data => {
        document.getElementById('generate-result').innerHTML = data;
    })
    .catch(error => {
        console.error('Error:', error);
    });
});
