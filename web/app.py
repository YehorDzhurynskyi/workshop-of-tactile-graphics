import requests
import json
import uuid
from PIL import Image
import os
import io

# TODO: clean up imports
from flask import Flask, render_template, request, abort, redirect, make_response
from flask_jwt_extended import JWTManager, jwt_required, get_jwt_identity, get_csrf_token, set_access_cookies, unset_jwt_cookies


app = Flask(__name__)
app.config['JWT_SECRET_KEY'] = '534c874dda9429b97edecbb5bae509f8'
app.config['JWT_TOKEN_LOCATION'] = ['cookies']

# If true this will only allow the cookies that contain your JWTs to be sent
# over https. In production, this should always be set to True
app.config["JWT_COOKIE_SECURE"] = False
app.config['JWT_COOKIE_CSRF_PROTECT'] = True

jwt = JWTManager(app)


@app.context_processor
@jwt_required(optional=True)
def inject_variables():

    current_user = get_jwt_identity()

    vars = {
        'appname': 'Майстерня тактильної графіки',
        'current_user': current_user
    }

    if current_user:
        access_token = request.cookies.get(app.config["JWT_ACCESS_COOKIE_NAME"])
        vars['csrf_token'] = get_csrf_token(access_token)

    return vars


@app.route("/")
@jwt_required(optional=True)
def index():

    headers = {}

    current_user = get_jwt_identity()
    if current_user:
        headers['Authorization'] = f'Bearer {request.cookies.get(app.config["JWT_ACCESS_COOKIE_NAME"])}'

    response = requests.get('http://webservice_tg_library:5002/api/models', headers=headers)

    if response.status_code != 200:
        abort(response.status_code)

    models = json.loads(response.text)

    return render_template('index.html', active_page='index', models=models)


@app.route("/library", methods=['GET'])
@jwt_required(optional=True)
def library():

    categories = request.args.getlist('categories')
    text_prompt = request.args.get('text_prompt', default='', type=str).strip()

    headers = {}

    current_user = get_jwt_identity()
    if current_user:
        headers['Authorization'] = f'Bearer {request.cookies.get(app.config["JWT_ACCESS_COOKIE_NAME"])}'

    params = {
        'text_prompt': text_prompt,
        'categories': categories
    }

    if True:
        response = requests.get('http://webservice_tg_library:5002/api/items', params=params, headers=headers)

        if response.status_code != 200:
            abort(response.status_code)

        text_json = json.loads(response.text)

        selected_categories = categories if len(categories) != 0 else text_json['categories']

    else:
        # TODO: delete in release
        text_json = {
            'items': [{
                'text_prompt': 'asdasd',
                'uploaded_at': 'assad',
                'categories': ['ss', 'hh'],
                'groups': [
                    {
                        'name': 'myname',
                        'brand_color': '0xff00ff'
                    }
                ]
            }],
            'categories': categories
        }

        selected_categories = categories

    return render_template('library.html',
                            active_page='library',
                            items=text_json['items'],
                            categories=text_json['categories'],
                            text_prompt=text_prompt,
                            selected_categories=selected_categories)


@app.route("/upload", methods=['POST'])
def upload():

    if not all(key in request.files for key in ['image']):
        abort(400)

    data = {
        'text_prompt': request.form.get('text-prompt', default='', type=str).strip()
    }
    img = request.files.get('image').read()

    img_path = f'/tmp/{uuid.uuid4()}.jpg'
    Image.open(io.BytesIO(img)).convert('RGB').save(img_path, 'JPEG')

    files = {
        'json': ('json_data', json.dumps(data), 'application/json'),
        'image': ('image.jpg', open(img_path, 'rb'), 'image/jpeg')
    }

    response = requests.post('http://webservice_tg_library:5002/api/items/upload', files=files)

    os.remove(img_path)

    if response.status_code != 200:
        abort(response.status_code)

    return redirect(request.referrer or '/')


@app.route("/generate", methods=['POST'])
def generate():

    # Зчитування даних, що були передані HTML формою
    text_prompt = request.form.get('text_prompt', default='').strip()
    uri = request.form.get('model_uri', default='').strip()

    # Перевірка коректності вводу даних користувачем
    if len(text_prompt) == 0 or len(uri) == 0:
        # Звітування про помилку "Bad Request" користувачу
        abort(400)

    payload = {
        'text_prompt': text_prompt,
        'uri': uri
    }

    response = requests.post('http://webservice_tg_generator:5001/api/generate', json=payload)

    if response.status_code != 200:
        abort(response.status_code)

    text_json = json.loads(response.text)

    return render_template('generate-result.html', uri=text_json['uri'])


@app.route("/login", methods=['GET', 'POST'])
def login():

    if request.method == 'GET':
        return render_template('login.html', appname='Майстерня тактильної графіки', active_page='login')

    if request.method == 'POST':

        data = json.dumps({
            'username': request.form.get('username'),
            'password': request.form.get('password')
        })

        response = requests.post('http://webservice_tg_library:5002/api/login', json=data)

        if response.status_code != 200:
            abort(response.status_code)

        access_token = json.loads(response.text)['access_token']

        response = make_response(redirect('/library'))
        set_access_cookies(response, access_token)

        return response


@app.route('/logout', methods=['GET'])
def logout():

    response = make_response(redirect(request.referrer or '/'))
    unset_jwt_cookies(response)

    return response
