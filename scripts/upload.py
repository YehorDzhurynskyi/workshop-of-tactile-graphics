import requests
import pandas as pd
import click
import json


CATEGORIES = {
    'animal': 'тварини',
    'plant': 'рослини',
    'pet': 'домашні тварини',
    'dinosaur': 'динозаври',
    'insect': 'комахи',
    'fish': 'риби',
    'tree': 'дерева',
    'flower': 'квітки',
}

SOURCES = {
    'aph': 'aph',
    'custom': 'public'
}


@click.command()
@click.option('--csv', help='The path to csv file.')
@click.option('--src', help='The path to image folder.')
def upload(csv, src):

    df = pd.read_csv(csv)

    print(len(df))

    for i in range(len(df)):

        filename = df['filename'][i].lower()
        filepath = f"{src}/{filename}".encode(encoding='utf-8').decode()
        filename = filename.encode(encoding='utf-8').decode()

        description_uk = df['description uk'][i].lower().encode(encoding='utf-8').decode()

        categories = df['category'][i].lower()
        categories = [CATEGORIES[category].encode(encoding='utf-8').decode() for category in categories.split(sep=';')]

        print(f'filename={filename}, categories={categories}')

        source = df['source'][i].lower()
        source = SOURCES[source].encode(encoding='utf-8').decode()

        data = {
            'text_prompt': description_uk,
            'categories': categories,
            'user_groups': [source],
            'generated_with': 'Публічна (версія 1.0.0)'
        }

        files = {
            'json': ('json_data', json.dumps(data), 'application/json'),
            'image': (filename, open(filepath, 'rb'), 'image/jpeg')
        }

        headers = {
            'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmcmVzaCI6ZmFsc2UsImlhdCI6MTcxMTAyMDIxNywianRpIjoiYTU3NTEyNmItMzZkMC00ZThmLTkwMDEtYzQ4MDk5NjU1OTI5IiwidHlwZSI6ImFjY2VzcyIsInN1YiI6ImFkbWluIiwibmJmIjoxNzExMDIwMjE3LCJjc3JmIjoiODVmZDRiZGUtNzA5OS00MzVlLThlNTQtOTRlYjc4MzhiN2Q2In0.USjjGAVmV9hBIv1UhQJ3R8snIYzFlHQ6mEeVnTbNFBw'
        }

        response = requests.post('http://127.0.0.1:5002/api/items/upload', files=files, headers=headers)
        assert response.status_code == 200

        print(f'{filepath} has been uploaded')


if __name__ == '__main__':
    upload()
