-- Tables:
CREATE TABLE IF NOT EXISTS users (
    user_id INTEGER PRIMARY KEY,

    acc_name TEXT NOT NULL UNIQUE,
    acc_password TEXT NOT NULL,

    first_name TEXT,
    last_name TEXT,

    email TEXT
);

CREATE TABLE IF NOT EXISTS user_groups (
    user_group_id INTEGER PRIMARY KEY,
    name TEXT UNIQUE,
    brand_color TEXT UNIQUE
);

CREATE TABLE IF NOT EXISTS user_user_groups (
    id INTEGER PRIMARY KEY,

    checkedin_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,

    user_id INTEGER NOT NULL,
    user_group_id INTEGER NOT NULL,

    FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (user_group_id) REFERENCES user_groups(user_group_id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS models (
    model_id INTEGER PRIMARY KEY,

    name TEXT NOT NULL UNIQUE,
    uri TEXT NOT NULL UNIQUE,

    uploaded_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS items (
    item_id INTEGER PRIMARY KEY,
    text_prompt TEXT,
    uri TEXT NOT NULL UNIQUE,

    uploaded_by_user_id INTEGER,
    uploaded_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,

    generated_with_model_id INTEGER,
    generated_at TIMESTAMP,

    FOREIGN KEY (uploaded_by_user_id) REFERENCES users(user_id) ON DELETE SET NULL ON UPDATE CASCADE,
    FOREIGN KEY (generated_with_model_id) REFERENCES models(model_id) ON DELETE SET NULL ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS item_categories (
    item_category_id INTEGER PRIMARY KEY,
    name TEXT UNIQUE
);

CREATE TABLE IF NOT EXISTS item_item_categories (
    id INTEGER PRIMARY KEY,

    item_id INTEGER NOT NULL,
    item_category_id INTEGER NOT NULL,

    FOREIGN KEY (item_id) REFERENCES items(item_id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (item_category_id) REFERENCES item_categories(item_category_id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS user_group_model_permissions (
    id INTEGER PRIMARY KEY,

    user_group_id INTEGER NOT NULL,
    model_id INTEGER NOT NULL,

    FOREIGN KEY (user_group_id) REFERENCES user_groups(user_group_id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (model_id) REFERENCES models(model_id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS user_group_item_permissions (
    id INTEGER PRIMARY KEY,

    user_group_id INTEGER NOT NULL,
    item_id INTEGER NOT NULL,

    FOREIGN KEY (user_group_id) REFERENCES user_groups(user_group_id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (item_id) REFERENCES items(item_id) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Triggers:
CREATE TRIGGER assign_new_user_group_to_admin
AFTER INSERT ON user_groups
BEGIN
    INSERT INTO user_user_groups (user_id, user_group_id)
    VALUES (1, NEW.user_group_id); -- admin id
END;

CREATE TRIGGER assign_new_user_to_public
AFTER INSERT ON users
BEGIN
    INSERT INTO user_user_groups (user_id, user_group_id)
    VALUES (NEW.user_id, 1); -- public group id
END;

CREATE TRIGGER assign_new_item_to_restricted
AFTER INSERT ON items
BEGIN
    INSERT INTO user_group_item_permissions (user_group_id, item_id)
    VALUES (2, NEW.item_id); -- restricted group id
END;

CREATE TRIGGER assign_new_model_to_restricted
AFTER INSERT ON models
BEGIN
    INSERT INTO user_group_model_permissions (user_group_id, model_id)
    VALUES (2, NEW.model_id); -- restricted group id
END;

CREATE TRIGGER remove_item_restricted_on_user_group_assign
AFTER INSERT ON user_group_item_permissions
WHEN NEW.user_group_id != 2 -- restricted group id
BEGIN
    DELETE FROM user_group_item_permissions WHERE user_group_item_permissions.user_group_id = 2 AND user_group_item_permissions.item_id = NEW.item_id; -- restricted group id
END;

CREATE TRIGGER remove_model_restricted_on_user_group_assign
AFTER INSERT ON user_group_model_permissions
WHEN NEW.user_group_id != 2 -- restricted group id
BEGIN
    DELETE FROM user_group_model_permissions WHERE user_group_model_permissions.user_group_id = 2 AND user_group_model_permissions.model_id = NEW.model_id; -- restricted group id
END;

-- Predefined rows:
INSERT OR IGNORE INTO users (acc_name, acc_password)
VALUES
    ("admin", "8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918"),
    ("user1", "0a041b9462caa4a31bac3567e0b6e6fd9100787db2ab433d96f6d178cabfce90"),
    ("user2", "6025d18fe48abd45168528f18a82e265dd98d421a7084aa09f61b341703901a3");

INSERT OR IGNORE INTO user_groups (name, brand_color)
VALUES
    ("public", "3252a8"),
    ("restricted", "f50c0c"),
    ("aph", "11ab23");

INSERT OR IGNORE INTO user_user_groups (user_id, user_group_id)
VALUES
    (2, 3);

INSERT OR IGNORE INTO models (name, uri)
VALUES
    ("Публічна (версія 1.0.0)", "568ba323d9a9dfab19b07cf59cc1bc8b"),
    ("aph-v1", "5ffefe22db652da368cda22b054adb07");

INSERT OR IGNORE INTO user_group_model_permissions (user_group_id, model_id)
VALUES
    (1, 1),
    (3, 2);

INSERT OR IGNORE INTO item_categories (name)
VALUES
    ("тварини"),
    ("рослини"),
    ("домашні тварини"),
    ("динозаври"),
    ("комахи"),
    ("риби"),
    ("дерева"),
    ("квітки");
