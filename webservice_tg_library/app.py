import os
import sqlite3
import hashlib
import logging
import json
import typing as t


from flask import Flask, request, jsonify, send_file, abort
from flask_jwt_extended import JWTManager, create_access_token, jwt_required, get_jwt_identity


app = Flask(__name__)
app.config['JWT_SECRET_KEY'] = '534c874dda9429b97edecbb5bae509f8'

# In production, this should always be not set.
app.config['JWT_ACCESS_TOKEN_EXPIRES'] = False


jwt = JWTManager(app)

TG_LIB_WORKDIR = os.getenv('TG_LIB_WORKDIR')


def connect_db():

    conn = sqlite3.connect(f'{TG_LIB_WORKDIR}/mnt/lib.db')
    conn.row_factory = sqlite3.Row

    return conn


def make_query_select_items(user_id: int, categories: t.List[str], text_prompt: str) -> str:

    user_id_where = 'perm.user_group_id = (SELECT user_group_id FROM user_groups WHERE name = "public")'
    if user_id is not None:
        user_id_where = f'grp_assign.user_id = {user_id}'

    categories_where = ''
    if categories is not None:

        categories = [f'\"{f}\"' for f in categories]
        categories = ', '.join(categories)

        categories_where = f"AND cat.name IN ({categories})"

    text_prompt_where = f"AND i.text_prompt LIKE \"%{text_prompt}%\"" if text_prompt is not None else ''

    return f"""
                SELECT
                    i.text_prompt AS "text_prompt",
                    i.uploaded_at AS "uploaded_at",
                    i.uri AS "uri",

                    (
                        SELECT
                            GROUP_CONCAT(cat.name, '<sep>')
                        FROM item_item_categories AS i_cat
                        JOIN item_categories AS cat ON cat.item_category_id = i_cat.item_category_id
                        WHERE i_cat.item_id = i.item_id
                    ) AS "categories",

                    (
                        SELECT
                            GROUP_CONCAT(grp.name || ' %brand-color%' || grp.brand_color, '<sep>')
                        FROM user_group_item_permissions AS perm
                        JOIN user_groups AS grp ON grp.user_group_id = perm.user_group_id
                        WHERE perm.item_id = i.item_id
                    ) AS "groups"

                FROM items AS i
                LEFT JOIN item_item_categories AS i_cat ON i_cat.item_id = i.item_id
                LEFT JOIN item_categories AS cat ON cat.item_category_id = i_cat.item_category_id
                JOIN user_group_item_permissions AS perm ON perm.item_id = i.item_id
                JOIN user_user_groups AS grp_assign ON grp_assign.user_group_id = perm.user_group_id
                WHERE
                {user_id_where}
                {text_prompt_where}
                {categories_where}
                GROUP BY i.item_id
            """


def make_query_select_categories(user_id: int):

    user_id_where = 'perm.user_group_id = (SELECT user_group_id FROM user_groups WHERE name = "public")'
    if user_id is not None:
        user_id_where = f'grp_assign.user_id = {user_id}'

    return f"""
            SELECT
                DISTINCT cat.name
            FROM item_categories AS cat
            JOIN item_item_categories AS i_cat ON i_cat.item_category_id = cat.item_category_id
            JOIN items AS i ON i.item_id = i_cat.item_id
            JOIN user_group_item_permissions AS perm ON perm.item_id = i.item_id
            JOIN user_user_groups AS grp_assign ON grp_assign.user_group_id = perm.user_group_id
            WHERE {user_id_where}
            ORDER BY cat.name ASC
            """


def make_response_items(result):

    for i in range(len(result)):
        categories = result[i]['categories']
        groups = result[i]['groups']

        result[i]['categories'] = categories.split('<sep>') if categories is not None else []
        result[i]['groups'] = groups.split('<sep>') if groups is not None else []

        groups = []
        for group in result[i]['groups']:
            grp = {}
            grp['name'] = group[:group.find('%brand-color%')].strip()
            grp['brand_color'] = f"#{group[group.find('%brand-color%') + len('%brand-color%'):].strip()}"

            groups.append(grp)

        result[i]['groups'] = groups

    return result


def make_response_categories(categories):

    result = []
    for category in categories:
        result.append(category['name'])

    return result


@jwt_required(optional=True)
def get_current_user_id():

    current_user = get_jwt_identity()

    if not current_user:
        return None

    conn = connect_db()
    cursor = conn.cursor()

    row = cursor.execute('SELECT user_id FROM users WHERE acc_name = ?', (current_user,)).fetchone()
    user_id = dict(row)['user_id']

    conn.close()

    return user_id


@app.route('/api/items', methods=['GET'])
def items():

    categories = request.args.getlist('categories')
    categories = categories if len(categories) != 0 else None

    text_prompt = request.args.get('text_prompt', default='', type=str).strip()
    text_prompt = text_prompt if len(text_prompt) != 0 else None

    user_id = get_current_user_id()

    conn = connect_db()
    cursor = conn.cursor()

    query = make_query_select_items(user_id, categories, text_prompt)
    rows = cursor.execute(query).fetchall()

    items = [dict(row) for row in rows]

    query = make_query_select_categories(user_id)
    rows = cursor.execute(query).fetchall()

    categories = [dict(row) for row in rows]

    conn.commit()
    conn.close()

    items = make_response_items(items)
    categories = make_response_categories(categories)

    response = {
        'items': items,
        'categories': categories
    }

    return jsonify(response)


@app.route('/api/items/<uri>', methods=['GET'])
def item(uri):

    # TODO: use token location: cookies

    # user_id = get_current_user_id()

    # conn = connect_db()

    # cursor = conn.cursor()

    # user_id_where = 'perm.user_group_id = (SELECT user_group_id FROM user_groups WHERE name = "public")'
    # if user_id is not None:
    #     user_id_where = f'grp_assign.user_id = {user_id}'

    # rows = cursor.execute(
    #     f'''
    #     SELECT uri
    #     FROM items AS i
    #     JOIN user_group_item_permissions AS perm ON perm.item_id = i.item_id
    #     JOIN user_user_groups AS grp_assign ON grp_assign.user_group_id = perm.user_group_id
    #     WHERE i.uri = ? AND {user_id_where}
    #     GROUP BY i.uri
    #     ''',
    #     (uri,)
    # ).fetchall()

    # rowcount = len(rows)

    # logging.warn(f'user_id={user_id} uri={uri} rowcount={rowcount}')

    # conn.close()

    # if rowcount == 0:
    #     abort(403)

    # assert rowcount == 1

    download = request.args.get('download', default=0, type=int)
    return send_file(f'{TG_LIB_WORKDIR}/mnt/images/{uri}.jpg', mimetype='image/jpeg', as_attachment=download)


@app.route('/api/items/upload', methods=['POST'])
def item_upload():

    if not all(key in request.files for key in ['image', 'json']):
        abort(400)

    data = json.loads(request.files.get('json').read().decode('utf-8'))

    text_prompt = data['text_prompt'] if 'text_prompt' in data else ''
    text_prompt = text_prompt.strip()
    text_prompt = text_prompt if len(text_prompt) != 0 else None

    img = request.files.get('image').read()
    uri = hashlib.md5(img).hexdigest()

    user_id = get_current_user_id()

    generated_with = data['generated_with'] if 'generated_with' in data else ''
    generated_with = generated_with.strip()
    generated_with = generated_with if len(generated_with) != 0 else None

    generated_at = data['generated_at'] if 'generated_at' in data else ''
    generated_at = generated_at.strip()
    generated_at = generated_at if len(generated_at) != 0 else None

    conn = connect_db()
    cursor = conn.cursor()

    cursor.execute("BEGIN TRANSACTION;")

    try:

        generated_with_model_id = None
        if generated_with is not None:

            row = cursor.execute(f'SELECT model_id AS id FROM models AS m WHERE m.name = ?', (generated_with,)).fetchone()
            generated_with_model_id = dict(row)['id']

        cursor.execute(
            """
            INSERT INTO items (text_prompt, uri, uploaded_by_user_id, generated_with_model_id, generated_at)
            VALUES (?, ?, ?, ?, ?)
            """,
            (text_prompt, uri, user_id, generated_with_model_id, generated_at)
        )

        item_id = cursor.lastrowid

        if user_id == 1:

            categories = data['categories'] if 'categories' in data else None
            if categories is not None:

                for category_name in categories:
                    cursor.execute(
                        """
                        INSERT INTO item_item_categories AS i_cat (item_id, item_category_id)
                        VALUES(?, (SELECT item_category_id FROM item_categories AS cat WHERE cat.name = ?))
                        """,
                        (item_id, category_name)
                    )

            user_groups = data['user_groups'] if 'user_groups' in data else None
            if user_groups is not None:

                for grp_name in user_groups:
                    cursor.execute(
                        """
                        INSERT INTO user_group_item_permissions AS perm (user_group_id, item_id)
                        VALUES((SELECT user_group_id FROM user_groups AS grp WHERE grp.name = ?), ?)
                        """,
                        (grp_name, item_id)
                    )


        with open(f'{TG_LIB_WORKDIR}/mnt/images/{uri}.jpg', 'wb') as f:
            f.write(img)

        conn.commit()

    except sqlite3.IntegrityError as e:

        logging.warn(f'Transaction rolled back due to an error: {e}! Returning existing URI...')
        conn.rollback()

    except Exception as e:

        logging.error(f'Transaction rolled back due to an error: {e}!')
        conn.rollback()

        abort(400)

    finally:
        conn.close()

    response = {
        'uri': uri
    }

    return jsonify(response)


@app.route('/api/models', methods=['GET'])
def models():

    user_id = get_current_user_id()

    conn = connect_db()

    cursor = conn.cursor()

    user_id_where = 'perm.user_group_id = (SELECT user_group_id FROM user_groups WHERE name = "public")'
    if user_id is not None:
        user_id_where = f'grp_assign.user_id = {user_id}'

    rows = cursor.execute(
        f'''
        SELECT name, uri
        FROM models AS m
        JOIN user_group_model_permissions AS perm ON perm.model_id = m.model_id
        JOIN user_user_groups AS grp_assign ON grp_assign.user_group_id = perm.user_group_id
        WHERE {user_id_where}
        GROUP BY m.name, m.uri
        '''
    ).fetchall()

    rows = [dict(row) for row in rows]

    conn.commit()
    conn.close()

    return jsonify(rows), 200


@app.route('/api/login', methods=['POST'])
def login():

    data = json.loads(request.json)

    username = data.get('username').strip()
    password = data.get('password').strip()

    conn = connect_db()

    cursor = conn.cursor()

    row = cursor.execute("SELECT acc_password FROM users WHERE acc_name = ?", (username,)).fetchone()
    password_ground_truth = dict(row)['acc_password']

    conn.close()

    if password_ground_truth == password:
        access_token = create_access_token(identity=username)
        return jsonify(access_token=access_token), 200
    else:
        return jsonify({'msg': 'Invalid credentials'}), 401
